// Ova biblioteka ovisi o webgl-math.js (trenutno samo zbog metode wgl$.lookAt),
// pa je istu potrebno uključiti ako se poziva metoda wgl$.lookAt.
// -----------------------------------------------------------------------------

wgl$.translation = function(dx, dy, dz) {
  return [[1,0,0,dx],[0,1,0,dy],[0,0,1,dz],[0,0,0,1]];
}

wgl$.scaling = function(sx, sy, sz) {
  return [[sx,0,0,0],[0,sy,0,0],[0,0,sz,0],[0,0,0,1]];
}

wgl$.rotateAroundXAxis = function(angle) {
  var c = Math.cos(angle);
  var s = Math.sin(angle);
  return [[1,0,0,0],[0,c,-s,0],[0,s,c,0],[0,0,0,1]];
}

wgl$.rotateAroundYAxis = function(angle) {
  var c = Math.cos(angle);
  var s = Math.sin(angle);
  return [[c,0,s,0],[0,1,0,0],[-s,0,c,0],[0,0,0,1]];
}

wgl$.rotateAroundZAxis = function(angle) {
  var c = Math.cos(angle);
  var s = Math.sin(angle);
  return [[c,-s,0,0],[s,c,0,0],[0,0,1,0],[0,0,0,1]];
}

wgl$.rotateAroundVector = function(angle, v) {
  v = v$.normalized(v$.elems(v));
  let s = Math.sin(angle);
  let c = Math.cos(angle);
  let u = 1-c;
  return [[v[0]*v[0]*u+c, v[0]*v[1]*u-v[2]*s, v[2]*v[0]*u+v[1]*s, 0],
          [v[0]*v[1]*u+v[2]*s, v[1]*v[1]*u+c, v[2]*v[1]*u-v[0]*s, 0],
          [v[0]*v[2]*u-v[1]*s, v[1]*v[2]*u+v[0]*s, v[2]*v[2]*u+c, 0],
          [0,0,0,1]];
}

// https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml
// Argumenti su tri 3D vektora (polja); postupak je malo modificiran jer mi se cini
// da prema postupku iz dokumentacije vektor s ne mora na kraju postati jedinicni a trebao bi
wgl$.lookAt = function(eye, center, up) {
  var f = v$.normalized(v$.sub(center, eye));
  var s = v$.normalized(v$.cross(f,v$.normalized(up)));
  var u = v$.cross(s,f);
  return [
    [s[0], s[1], s[2], -eye[0]*s[0]-eye[1]*s[1]-eye[2]*s[2]],
    [u[0], u[1], u[2], -eye[0]*u[0]-eye[1]*u[1]-eye[2]*u[2]],
    [-f[0], -f[1], -f[2], eye[0]*f[0]+eye[1]*f[1]+eye[2]*f[2]],
    [0, 0, 0, 1]
  ];
}

